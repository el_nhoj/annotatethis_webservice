# README #

### What is it? ###
Source code for the 'Annotate This!' web service.

### Dependencies ###

* build.properties file - edit it so it points to your Axis2 directory
* Please ensure the following JAR files have been included in the project's build path: Axis2 JARs, derby.jar, derbyclient.jar, derbynet.jar
* IMPORTANT: ALSO INCLUDE THESE JARS IN ECLIPSE'S CLASS PATH

### How do I get set up? ###

* Firstly, we need to initialize the database. This uses Derby's network server listener program, so in the /bin directory of your Derby folder run "startNetworkServer.bat".
* In the Eclipse project, go to "model.database" package and double-click on "DatabaseInitialization.java" file and change line 46 to point to your Derby database name - by default, it will be set to "MyDB".
* Now right-click on "DatabaseInitialization.java" and run the program to initialize the database and create/insert dummy data. With any luck, it should execute with no problems.
* Dummy user accounts are (username/password): jle/test123, pcollingwood/test456
* Now just build the service .AAR file and upload to Tomcat and generate the stubs using the build.xml file from the IIDE-Assignment-02 web project.

### Contributors ###
* John Le
* Petrina Collingwood