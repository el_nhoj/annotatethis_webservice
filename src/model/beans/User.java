package model.beans;

public class User {
	
	/**
	 * Server side class for user with standard constructors, getters and setters
	 */
	private String userId; //email
	private String name;
	private String username;
	private String password;
//	private String[] annotationIds;
	
	public User() {
	}
	
	public User(String userId, String name, String username, String password) {
		this.userId = userId;
		this.name = name;
		this.username = username;
		this.password = password;
	}
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUsername(){
		return username;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
//	public String[] getAnnotationIds() {
//		return annotationIds;
//	}
	
//	public void setAnnotationIds(String[] annotationIds) {
//		this.annotationIds = annotationIds;
//	}

}