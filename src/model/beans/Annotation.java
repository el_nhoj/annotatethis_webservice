package model.beans;

public class Annotation {
	/**
	 * Server side class for annotation with standard constructors, getters and setters
	 */

	private String annotationId;
	private String docId;
	private String userId;
	private String annotationContent;
	private int startPosition;
	private int endPosition;
		
	public Annotation() {
	}
	public Annotation(String annotationId, String docId, String userId, String annotationContent, int startPosition, int endPosition) {
		this.annotationId = annotationId;
		this.docId = docId;
		this.userId = userId;
		this.annotationContent = annotationContent;
		this.startPosition = startPosition;
		this.endPosition = endPosition;
	}
	public String getAnnotationId() {
		return annotationId;
	}
	public void setAnnotationId(String annotationId) {
		this.annotationId = annotationId;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getAnnotationContent() {
		return annotationContent;
	}
	public void setAnnotationContent(String annotationContent) {
		this.annotationContent = annotationContent;
	}
	public int getStartPosition() {
		return startPosition;
	}
	public void setStartPosition(int startPosition) {
		this.startPosition = startPosition;
	}
	public int getEndPosition() {
		return endPosition;
	}
	public void setEndPosition(int endPosition) {
		this.endPosition = endPosition;
	}
}