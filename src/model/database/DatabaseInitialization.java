package model.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import java.util.Properties;

import org.apache.log4j.Logger;

public class DatabaseInitialization {

	public static void main(String[] args) throws Exception {
		DatabaseInitialization appDB = new DatabaseInitialization();
		appDB.createAndPopulate();
	}

	// Method to initialize and populate application database
	public void createAndPopulate() {

		// Use log4j for simple logging
		final Logger logger = Logger.getLogger("dbConnection");

		// Put DB operations in separate thread
		new Thread(new Runnable() {
			public void run() {
				// Set driver and user properties
				String driver = "org.apache.derby.jdbc.ClientDriver";

				String protocol = "jdbc:derby://localhost:1527/";
				// String dbName = "C:/Users/John/MyDB";
				// User connection properties
				// Properties props = new Properties();
				// props.put("user", "test");
				// props.put("password", "test");

				try {
					// Instantiate the driver
					Class.forName(driver).newInstance();

					// Create a connection
					// Connection con = DriverManager.getConnection(protocol
					// + dbName + ";create=true", props);
					Connection con = DriverManager.getConnection(protocol
							+ "MyDB;create=true");
					Statement st = con.createStatement();
				
					// Drop tables if they already exist					
					st.executeUpdate("DROP TABLE MyDB.Annotation");
					logger.info("*** Deleted TABLE: MyDB.Annotation");
					st.executeUpdate("DROP TABLE MyDB.Customer");
					logger.info("*** Deleted TABLE: MyDB.Customer");
	
					// Create table Customer
					st.executeUpdate("CREATE TABLE MyDB.Customer (id VARCHAR(15) NOT NULL, name VARCHAR(70) NOT NULL, username VARCHAR(25) NOT NULL, password VARCHAR(128) NOT NULL, PRIMARY KEY (id))");
					logger.info("*** Created table: MyDB.Customer");
					
					// Create table Annotation
					st.executeUpdate("CREATE TABLE MyDB.Annotation (id VARCHAR(100) NOT NULL, DocID VARCHAR(50) NOT NULL, UserID VARCHAR(15) NOT NULL, AnnotationContent VARCHAR(200) NOT NULL, StartPosition INTEGER, EndPosition INTEGER, PRIMARY KEY (id), FOREIGN KEY (UserID) REFERENCES MyDB.Customer (id))");
					logger.info("*** Created table: MyDB.Annotation");
					
					// Add records to Customer
					st.executeUpdate("INSERT INTO MyDB.Customer VALUES ('UID001','John Le', 'jle', 'test123')");
					st.executeUpdate("INSERT INTO MyDB.Customer VALUES ('UID002','Petrina Collingwood', 'pcollingwood', 'test456')");
					logger.info("*** Inserted default records: MyDB.Customer");

					// Query and display results from each table
					ResultSet rs = st.executeQuery("SELECT * FROM MyDB.Customer");
					logger.info("*** Query results: MyDB.Customer");
					while (rs.next()) {
						logger.info("User ID: " + rs.getString(1));
						logger.info("User Full Name: " + rs.getString(2));
						logger.info("User Username: " + rs.getString(3));
						logger.info("User Password: " + rs.getString(4));
					}

					// Release resources
					rs.close();
					st.close();
					con.close();

				} catch (SQLException sqlEx) {
					while (sqlEx != null) {
						logger.error("[SQLException] " + "SQLState: "
								+ sqlEx.getSQLState() + ", Message: "
								+ sqlEx.getMessage() + ", Vendor: "
								+ sqlEx.getErrorCode());
						sqlEx = sqlEx.getNextException();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}

			}
		}).start();
	}

}
