package annotation;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.UUID;

import org.apache.log4j.Logger;

import model.beans.Annotation;
import model.beans.User;

public class AnnotationService {
	private final Logger logger = Logger.getLogger("dbConnection");
	private String driver = "org.apache.derby.jdbc.ClientDriver";
	private String protocol = "jdbc:derby://localhost:1527/";
	private Connection con;
	private Statement st;

	// Method to create annotations
	public String createAnnotation(String userID, String docID, String content,
			int startPosition, int endPosition) {
		String annotationId = UUID.randomUUID().toString();

		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB");
			st = con.createStatement();

			String SQL = "INSERT INTO MyDB.Annotation (id, DocID, UserID, AnnotationContent, StartPosition, EndPosition) VALUES (?, ?, ?, ?, ?, ?)";
			java.sql.PreparedStatement statement = con.prepareStatement(SQL);
			statement.setString(1, annotationId);
			statement.setString(2, docID);
			statement.setString(3, userID);
			statement.setString(4, content);
			statement.setInt(5, startPosition);
			statement.setInt(6, endPosition);
			statement.executeUpdate();
			statement.close();
			con.close();
			return annotationId;
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}
		return "";
	}

	// Method to retrieve annotations
	public Annotation[] retrieveAnnotations(String docID) {
		Annotation annotations[];
		ArrayList<Annotation> annotationList = new ArrayList<Annotation>();
		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB");
			st = con.createStatement();

			String query = "SELECT * FROM MyDB.Annotation WHERE DocID = ?";
			java.sql.PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, docID);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				annotationList.add(new Annotation(rs.getString("id"), rs
						.getString("DocID"), rs.getString("UserID"), rs
						.getString("AnnotationContent"), rs
						.getInt("StartPosition"), rs.getInt("EndPosition")));
			}
			con.close();
			st.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}
		annotations = annotationList.toArray(new Annotation[0]);
		return annotations;
	}

	// Method to check if user is valid
	public User login(String username, String password) {
		User user = null;

		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB");
			st = con.createStatement();

			ResultSet rs = st.executeQuery("SELECT * FROM MyDB.Customer");

			// Loop through result set and try to find a match
			while (rs.next()) {
				String un = rs.getString(3);
				String pw = rs.getString(4);

				if (un.equals(username) && pw.equals(password)) {
					user = new User();
					user.setUserId(rs.getString(1).toString());
					user.setName(rs.getString(2).toString());
					user.setUsername(rs.getString(3).toString());
					user.setPassword(rs.getString(4).toString());
				}
			}

			// Release resources
			rs.close();
			st.close();
			con.close();
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}
		return user;
	}

	// get all users
	public User[] getUsers() {
		User[] users = null;
		ArrayList<User> usersList = new ArrayList<User>();

		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB");
			st = con.createStatement();

			ResultSet rs = st.executeQuery("SELECT * FROM MyDB.Customer");

			// Convert to objects
			while (rs.next()) {
				usersList.add(new User(rs.getString("ID"),
						rs.getString("NAME"), rs.getString("USERNAME"), rs
								.getString("PASSWORD")));
			}
			// Release resources
			rs.close();
			st.close();
			con.close();
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}
		users = usersList.toArray(new User[0]);
		return users;
	}

	public Annotation retrieveAnnotationById(String annotationId) {
		Annotation annotation = null;
		try {
			// Instantiate driver
			Class.forName(driver).newInstance();

			con = DriverManager.getConnection(protocol + "MyDB");
			st = con.createStatement();

			String query = "SELECT * FROM MyDB.Annotation WHERE id = ?";
			java.sql.PreparedStatement ps = con.prepareStatement(query);
			ps.setString(1, annotationId);

			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				annotation = new Annotation(rs.getString("id"), rs
						.getString("DocID"), rs.getString("UserID"), rs
						.getString("AnnotationContent"), rs
						.getInt("StartPosition"), rs.getInt("EndPosition"));
			}
			con.close();
			st.close();
			ps.close();
			rs.close();
		} catch (Exception e) {
			logger.info("Error" + e.toString());
		}
		return annotation;
	}
}